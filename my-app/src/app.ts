import {Router, RouterConfiguration} from 'aurelia-router';
import {PLATFORM} from 'aurelia-pal';

export class App {
   router: Router;

   configureRouter(config: RouterConfiguration, router: Router){
      config.title = 'Online Users';
      config.options.pushState = true;
      config.options.root = '/';
      config.map([
        { route: '',  moduleId: PLATFORM.moduleName('user-app'), name:'user' }
      ]);

      this.router = router;
  }
}



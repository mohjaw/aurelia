import {autoinject} from 'aurelia-framework';
import { User } from './user-class';
import {HttpClient} from 'aurelia-fetch-client';



@autoinject
export class MyApp {
  currentDate;
  heading;
  selectedId = 0;
  users: any;
  userName: string;
  userEmail: string;
  userWorkplace: string;
  userTime: string;


  constructor(private http: HttpClient) {
    this.update();
    setInterval(() => this.update(), 1000);
    this.heading = "Welcome in Online Users...";
    this.users = this.getUsersFromStorage();
    this.userName = '';
    this.userEmail = '';
    this.userWorkplace = '';
    this.userTime = '';

  }

  update() {
    this.currentDate = new Date();
  }



  getUsersFromStorage() {
    let users;
    if(localStorage.getItem('users') === null) {
      users = [];
    } else {
      users = JSON.parse(localStorage.getItem('users'));
    }

    return users;
  }



  addUser() {
    if(this.userName && this.userEmail && this.userWorkplace && this.userTime) {
      this.users.push(new User(this.userName, this.userEmail, this.userWorkplace,  this.userTime));

      // Store in LS
      this.storeUser(this.userName, this.userEmail, this.userWorkplace, this.userTime);

      // Clear Fields
      this.userName = '';
      this.userEmail = '';
      this.userWorkplace = '';
      this.userTime = '';

    }
  }



  storeUser(name: string, email: string, workplace: string, time: string){
    let users;
    if(localStorage.getItem('users') === null){
      users = [];
    } else {
      users = JSON.parse(localStorage.getItem('users'));
    }

    users.push({name: name, email: email, workplace: workplace, time: time});
    localStorage.setItem('users', JSON.stringify(users));
  }

  removeUser(user: any) {
    let index = this.users.indexOf(user);
    if(index !== -1) {
      this.users.splice(index, 1);
    }
    this.removeUserFromStorage(index);
  }

  removeUserFromStorage (index: any){
    let users = JSON.parse(localStorage.getItem('users'));

    users.splice(index, 1);

    localStorage.setItem('users', JSON.stringify(users));
  }

}

//test Server with Vertx
const socket = new WebSocket('ws://localhost:8081');
const sendNextMessage = function() {
  setTimeout(function(){
      socket.send("Hello server!");
      sendNextMessage();
  }, 3000);
}
socket.onopen = function (event) {
   sendNextMessage();
};
socket.onmessage = function (event) {
   console.log(event);
}
socket.onerror = function (event) {
   console.log(event);
}
socket.onclose = function (event) {
   console.log(event);
}





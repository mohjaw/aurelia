package com.vertx.App;



import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.StaticHandler;

public class App {
    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        HttpServer httpServer = vertx.createHttpServer();

        Router router = Router.router(vertx);

        router.route("/static/*").handler(StaticHandler.create("webroot").setCachingEnabled(false));

        httpServer.webSocketHandler(ws -> {
            ws.frameHandler(event -> {
               var data = event.textData();
                System.out.println(data);
                ws.writeTextMessage("Hallo zurück");
            });
        });

        httpServer.requestHandler(router);
        httpServer.listen(8081);
    }

}
